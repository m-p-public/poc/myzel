import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import {createRouter, createWebHashHistory} from 'vue-router'

import PrimeVue from 'primevue/config'
import Button from 'primevue/button'
import Slider from 'primevue/slider'
import Panel from 'primevue/panel'
import Knob from 'primevue/knob'
import Card from 'primevue/card'
import Checkbox from 'primevue/checkbox'
import Dropdown from 'primevue/dropdown';
import Tooltip from 'primevue/tooltip';

//theme
import 'primevue/resources/themes/lara-light-indigo/theme.css'

import '/node_modules/primeflex/primeflex.css'

//core
import 'primevue/resources/primevue.min.css'
import TheNet from '@/components/TheNet.vue';
import PixiJs from "@/components/PixiJs.vue";

const routes = [
  { path: '/', component: TheNet },
  { path: '/pixijs', component: PixiJs },
]

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHashHistory(),
  routes, // short for `routes: routes`
})

const app = createApp(App)
app.use(PrimeVue)
app.use(router)
app.component('Button', Button)
app.component('Slider', Slider)
app.component('Panel', Panel)
app.component('Knob', Knob)
app.component('Card', Card)
app.component('Checkbox', Checkbox)
app.component('Dropdown', Dropdown)
app.directive('tooltip', Tooltip);
app.mount('#app')
