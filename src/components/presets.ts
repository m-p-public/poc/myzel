import type { TreeOptions } from '@/lib/Tree'

type Preset = {
  name: string
  values: TreeOptions & { seedCount?: number; randomSeedPosition?: boolean }
}

const presets: Preset[] = [
  {
    name: 'Bushes',
    values: {
      maxEdgeCount: 9123,
      speed: 0.2,
      edgeLength: 36,
      minAngle: 30,
      maxAngle: 43,
      minFanout: 2,
      maxFanout: 2,
      allowOverlap: true,
      gravity: true,
      mouseEffectRange: 0,
      hasMouseControl: false,
      hasMouseBudGeneration: false,
      seedCount: 3,
      randomSeedPosition: false
    }
  },
  {
    name: 'Spirals',
    values: {
      maxEdgeCount: 9123,
      speed: 0.2,
      edgeLength: 36,
      minAngle: 30,
      maxAngle: 43,
      minFanout: 2,
      maxFanout: 2,
      allowOverlap: false,
      gravity: false,
      mouseEffectRange: 0,
      hasMouseControl: false,
      hasMouseBudGeneration: false,
      seedCount: 1,
      randomSeedPosition: false
    }
  },
  {
    name: 'Cities',
    values: {
      maxEdgeCount: 3000,
      speed: 0.2,
      edgeLength: 20,
      minAngle: 179,
      maxAngle: 180,
      minFanout: 2,
      maxFanout: 2,
      seedCount: 3,
      randomSeedPosition: true,
      allowOverlap: false,
      gravity: false,
      mouseEffectRange: 0,
      hasMouseControl: false,
      hasMouseBudGeneration: false
    }
  },
  {
    name: 'Growth by touch',
    values: {
      maxEdgeCount: 5697,
      speed: 0.2,
      edgeLength: 36,
      minAngle: 30,
      maxAngle: 43,
      minFanout: 2,
      maxFanout: 2,
      mouseEffectRange: 43,
      allowOverlap: false,
      gravity: false,
      hasMouseControl: true,
      hasMouseBudGeneration: false,
      seedCount: 3
    }
  }
]

export default presets
