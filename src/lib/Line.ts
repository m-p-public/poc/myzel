export interface Point {
  x: number
  y: number
}

export class Line {
  public startPoint: Point
  public endPoint: Point

  constructor(startPoint: Point, endPoint: Point) {
    this.startPoint = startPoint
    this.endPoint = endPoint
  }

  scale(factor: number): Line {
    const scaledEndPoint: Point = {
      x: this.startPoint.x + (this.endPoint.x - this.startPoint.x) * factor,
      y: this.startPoint.y + (this.endPoint.y - this.startPoint.y) * factor
    }

    return new Line(this.startPoint, scaledEndPoint)
  }

  toLength(newLength: number): Line {
    const dx = this.endPoint.x - this.startPoint.x
    const dy = this.endPoint.y - this.startPoint.y
    const currentLength = Math.sqrt(dx * dx + dy * dy)

    const scaleFactor = newLength / currentLength

    const newEndPoint: Point = {
      x: this.startPoint.x + dx * scaleFactor,
      y: this.startPoint.y + dy * scaleFactor
    }

    return new Line(this.startPoint, newEndPoint)
  }

  /**
   * Creates a new Line that continues from the endPoint of the original line, at a specified angle relative to the original line.
   * The continuation line will have the same length as the original line.
   *
   * @param angle - The angle for the continuation, relative to the direction of the original line.
   *                This is measured in radians, with positive values indicating counterclockwise rotation.
   * @returns A new Line instance representing the continued line.
   */
  continueFrom(angle: number): Line {
    const dx = this.endPoint.x - this.startPoint.x
    const dy = this.endPoint.y - this.startPoint.y
    const length = Math.sqrt(dx * dx + dy * dy)

    // Calculate the angle of the original line
    const originalAngle = Math.atan2(dy, dx)

    // Calculate the absolute angle for the new line
    const absoluteAngle = originalAngle + angle

    const continuedEndPoint: Point = {
      x: this.endPoint.x + length * Math.cos(absoluteAngle),
      y: this.endPoint.y + length * Math.sin(absoluteAngle)
    }

    return new Line(this.endPoint, continuedEndPoint)
  }

  /**
   * Creates a new Line that continues from a given point, at a specified angle relative to the original line.
   * The continuation line will have the same length as the original line.
   *
   * @param continuationPoint - The point from which to continue the line.
   * @param angle - The angle for the continuation, relative to the direction of the original line.
   *                This is measured in radians, with positive values indicating counterclockwise rotation.
   * @returns A new Line instance representing the continued line.
   */
  continueFromPoint(continuationPoint: Point, angle: number): Line {
    const dx = this.endPoint.x - this.startPoint.x
    const dy = this.endPoint.y - this.startPoint.y
    const length = Math.sqrt(dx * dx + dy * dy)

    // Calculate the angle of the original line
    const originalAngle = Math.atan2(dy, dx)

    // Calculate the absolute angle for the new line
    const absoluteAngle = originalAngle + angle

    const continuedEndPoint: Point = {
      x: continuationPoint.x + length * Math.cos(absoluteAngle),
      y: continuationPoint.y + length * Math.sin(absoluteAngle)
    }

    return new Line(continuationPoint, continuedEndPoint)
  }

  isTouching(line: Line): boolean {
    // touching on the endpoints does not count
    if (
      (line.startPoint.x === this.endPoint.x && line.startPoint.y === this.endPoint.y) ||
      (line.startPoint.x === this.startPoint.x && line.startPoint.y === this.startPoint.y) ||
      (this.startPoint.x === line.endPoint.x && this.startPoint.y === line.endPoint.y) ||
      (this.endPoint.x === line.endPoint.x && this.endPoint.y === line.endPoint.y)
    ) {
      return false
    }

    const crossProduct = (point1: Point, point2: Point, point3: Point) => {
      return (
        (point2.y - point1.y) * (point3.x - point2.x) -
        (point2.x - point1.x) * (point3.y - point2.y)
      )
    }

    const differentSides = (line1: Line, line2: Line) => {
      const cp1 = crossProduct(line1.startPoint, line1.endPoint, line2.startPoint)
      const cp2 = crossProduct(line1.startPoint, line1.endPoint, line2.endPoint)
      return cp1 * cp2 <= 0
    }

    return differentSides(this, line) && differentSides(line, this)
  }

  isTheSameAs(line: Line): boolean {
    return (
      this.startPoint.x === line.startPoint.x &&
      this.startPoint.y === line.startPoint.y &&
      this.endPoint.x === line.endPoint.x &&
      this.endPoint.y === line.endPoint.y
    )
  }

  angleBetween(line: Line): number {
    const deltaX1 = this.endPoint.x - this.startPoint.x
    const deltaY1 = this.endPoint.y - this.startPoint.y

    const deltaX2 = line.endPoint.x - line.startPoint.x
    const deltaY2 = line.endPoint.y - line.startPoint.y

    // Calculating the direction (unit vectors) of the lines
    const magnitude1 = Math.sqrt(deltaX1 ** 2 + deltaY1 ** 2)
    const magnitude2 = Math.sqrt(deltaX2 ** 2 + deltaY2 ** 2)

    const directionX1 = deltaX1 / magnitude1
    const directionY1 = deltaY1 / magnitude1

    const directionX2 = deltaX2 / magnitude2
    const directionY2 = deltaY2 / magnitude2

    // Dot product of direction vectors gives the cosine of the angle between them
    const dotProduct = directionX1 * directionX2 + directionY1 * directionY2

    // Clamp value between -1 and 1 to avoid invalid values due to precision errors
    const clampedValue = Math.max(-1, Math.min(1, dotProduct))

    // Get the angle from the cosine value
    const angle = Math.acos(clampedValue)

    return angle
  }

  // returns the distance between the line and the point
  distanceTo(point: Point): number {
    const x0 = point.x
    const y0 = point.y
    const x1 = this.startPoint.x
    const y1 = this.startPoint.y
    const x2 = this.endPoint.x
    const y2 = this.endPoint.y

    // Check if the line segment is more like a point
    if (x1 === x2 && y1 === y2) {
      // Return the distance from the point to the start (or end) point
      return Math.sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)
    }

    // Calculate the perpendicular distance from the point to the line
    const numerator = Math.abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1))
    const denominator = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    // Check if the perpendicular falls within the line segment
    const dotProduct = (x0 - x1) * (x2 - x1) + (y0 - y1) * (y2 - y1)
    if (dotProduct < 0) {
      // The perpendicular falls outside the line segment, towards the start point
      return Math.sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)
    }

    const squaredLengthBA = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)
    if (dotProduct > squaredLengthBA) {
      // The perpendicular falls outside the line segment, towards the end point
      return Math.sqrt((x2 - x0) ** 2 + (y2 - y0) ** 2)
    }

    // The perpendicular falls inside the line segment
    return numerator / denominator
  }
}
