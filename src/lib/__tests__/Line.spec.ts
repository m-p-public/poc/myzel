import { describe, it, expect } from 'vitest'
import { Line, type Point } from '../Line'

describe('Line', () => {
  it('scales correctly', () => {
    const startPoint: Point = { x: 0, y: 0 }
    const endPoint: Point = { x: 2, y: 2 }
    const line = new Line(startPoint, endPoint)

    const factor = 2
    const scaledLine = line.scale(factor)

    // Expect the scaled end point to be at {x: 4, y: 4}
    expect(scaledLine).toEqual(new Line(startPoint, { x: 4, y: 4 }))
  }),
    it('create a new line which continues straightly', () => {
      const startPoint: Point = { x: 0, y: 0 }
      const endPoint: Point = { x: 1, y: 1 }
      const line = new Line(startPoint, endPoint)

      const angle = 0 // the continuation is straight
      const continuedLine = line.continueFrom(angle)

      expect(continuedLine).toEqual(new Line({ x: 1, y: 1 }, { x: 2, y: 2 }))
    })

  it('creates a new line which continues back to the start point', () => {
    const startPoint: Point = { x: 0, y: 0 }
    const endPoint: Point = { x: 1, y: 1 }
    const line = new Line(startPoint, endPoint)

    const angle = Math.PI
    const continuedLine = line.continueFrom(angle)

    expect(continuedLine.endPoint.x).toBeCloseTo(0)
    expect(continuedLine.endPoint.y).toBeCloseTo(0)
  })

  it('creates a new line which continues in a right angle', () => {
    const startPoint: Point = { x: 0, y: 0 }
    const endPoint: Point = { x: 1, y: 1 }
    const line = new Line(startPoint, endPoint)

    const angle = Math.PI / 2 // The continuation is at a right angle (counterclockwise)
    const continuedLine = line.continueFrom(angle)

    expect(continuedLine.endPoint.x).toBeCloseTo(0)
    expect(continuedLine.endPoint.y).toBeCloseTo(2)
  })
  
  it('creates a new line which continues in a the same direction from a different point', () => {
    const startPoint: Point = { x: 0, y: 0 }
    const endPoint: Point = { x: 1, y: 1 }
    const line = new Line(startPoint, endPoint)

    const angle = 0 // The continuation is at a right angle (counterclockwise)
    const continuedLine = line.continueFromPoint({ x: 0, y: 0 }, angle)

    expect(continuedLine.endPoint.x).toBeCloseTo(1)
    expect(continuedLine.endPoint.y).toBeCloseTo(1)
  })

    
  it('creates a new line which continues in a right angle from a different point', () => {
    const startPoint: Point = { x: 0, y: 0 }
    const endPoint: Point = { x: 1, y: 1 }
    const line = new Line(startPoint, endPoint)

    const angle = Math.PI / 2 // The continuation is at a right angle (counterclockwise)
    const continuedLine = line.continueFromPoint({ x: 0, y: 0 }, angle)

    expect(continuedLine.endPoint.x).toBeCloseTo(-1)
    expect(continuedLine.endPoint.y).toBeCloseTo(1)
  })

  it('changes the length of the line to 1 while keeping the start point the same', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const newLength = 1 * Math.sqrt(2);
    const newLine = line.toLength(newLength);

    expect(newLine).toEqual(new Line({ x: 0, y: 0 }, { x: 1, y: 1 }))
  });

  it('detects identical lines', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    expect(line.isTheSameAs(line)).toBe(true);
  })

  it('detects identical lines if its two differen objects', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const line2 = new Line(startPoint, endPoint);

    expect(line.isTheSameAs(line2)).toBe(true);
  })

  it('detects non-identical lines', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const startPoint2: Point = { x: 0, y: 0 };
    const endPoint2: Point = { x: 2, y: 3 };
    const line2 = new Line(startPoint2, endPoint2);

    expect(line.isTheSameAs(line2)).toBe(false);
  })

  it ('detects if two lines are intersecting', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const otherStartPoint: Point = { x: 0, y: 2 };
    const otherEndPoint: Point = { x: 2, y: 0 };  
    const otherLine = new Line(otherStartPoint, otherEndPoint);

    expect(line.isTouching(otherLine)).toBe(true);
  })

  it ('detects if two lines are intersecting if they are far away', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const otherStartPoint: Point = { x: 3, y: 2 };
    const otherEndPoint: Point = { x: 5, y: 0 };  
    const otherLine = new Line(otherStartPoint, otherEndPoint);

    expect(line.isTouching(otherLine)).toBe(false);
  })

  it ('detects if two lines are intersecting if they are just touching one end', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const otherStartPoint: Point = { x: 2, y: 2 };
    const otherEndPoint: Point = { x: 3, y: 3 };  
    const otherLine = new Line(otherStartPoint, otherEndPoint);

    expect(line.isTouching(otherLine)).toBe(false);
  })

  it('returns an angle 0 for to lines which are parallel', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const otherStartPoint: Point = { x: 0, y: 0 + 5 };
    const otherEndPoint: Point = { x: 2, y: 2 + 5 };  
    const otherLine = new Line(otherStartPoint, otherEndPoint);

    expect(line.angleBetween(otherLine)).toBeCloseTo(0);
  })

  it('returns an angle Pi for to lines which are in opposite directions', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);

    const otherStartPoint: Point = { x: 0, y: 0 + 5 };
    const otherEndPoint: Point = { x: 2, y: 2 + 5 };  
    const otherLine = new Line(otherEndPoint, otherStartPoint);

    expect(line.angleBetween(otherLine)).toBeCloseTo(Math.PI);
  })

  it('returns an angle of 90° for two lines which are orthogonal', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 0 };
    const line = new Line(startPoint, endPoint);

    const otherStartPoint: Point = { x: 0, y: 0 };
    const otherEndPoint: Point = { x: 0, y: 2 };  
    const otherLine = new Line(otherEndPoint, otherStartPoint);

    expect(line.angleBetween(otherLine)).toBeCloseTo(Math.PI / 2);
  })

  it('it returns a disance of zero if the point is on the line', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 0 };
    const line = new Line(startPoint, endPoint);
    expect(line.distanceTo({ x:0, y:0 })).toBeCloseTo(0);
  })

  it('it returns a distance of zero if the point is on the line', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);
    expect(line.distanceTo({ x:1, y:1 })).toBeCloseTo(0);
  })

  it('the perpendicular falls outside the line segment, close to the startPoint', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);
    expect(line.distanceTo({ x:-10, y:0 })).toBeCloseTo(10);
  })

  it('the perpendicular falls outside the line segment, close to the startPoint', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);
    expect(line.distanceTo({ x:-10, y:0 })).toBeCloseTo(10);
  })

  it('it returns a disance of 10 if the perpendicular falls outside the line segment, close to the endPoint', () => {
    const startPoint: Point = { x: 0, y: 0 };
    const endPoint: Point = { x: 2, y: 2 };
    const line = new Line(startPoint, endPoint);
    expect(line.distanceTo({ x:2, y:3 })).toBeCloseTo(1);
  })
})
