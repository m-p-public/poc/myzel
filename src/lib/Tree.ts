import { Line, type Point } from '@/lib/Line'
import {
  SVG,
  Svg,
  extend as SVGextend,
  Line as SVGLine,
  Element as SVGElement
} from '@svgdotjs/svg.js'
import { throttle } from 'lodash'

export type Edge = {
  id: number
  line: Line
  completion: number
  done: boolean
  svgLine: SVGLine | null
  generation: number
}

export type TreeOptions = {
  maxEdgeCount: number
  speed: number
  edgeLength: number
  minAngle: number
  maxAngle: number
  minFanout: number
  maxFanout: number
  mouseEffectRange: number
  allowOverlap: boolean
  gravity: boolean
  hasMouseControl: boolean
  hasMouseBudGeneration: boolean
}

export class Tree {
  edges: Edge[] = []
  options: TreeOptions
  done: boolean = false
  draw: Svg
  mousePosition: Point | null = null
  processTouch: (edge: Edge, m: Point) => void

  constructor(draw: Svg, treeOptions: TreeOptions) {
    this.edges = []
    this.options = treeOptions
    this.draw = draw

    // This function processes the touch event on an edge
    this.processTouch = throttle((edge: Edge, touchpoint: Point) => {
      if (this.options.hasMouseBudGeneration) {
        if (edge.svgLine) {
          edge.svgLine.stroke({ color: '#aa2222', width: 2 })

          this.createBud(edge, touchpoint)
        }
        setTimeout(() => {
          if (edge.svgLine) {
            edge.svgLine.stroke({ color: '#000000', width: 1 })
          }
        }, 1000)
      }
    }, 50)
  }
  setMousePosition(mousePosition: Point) {
    this.mousePosition = mousePosition

    this.edges
      .filter((edge) => edge.line.distanceTo(mousePosition) < 5)
      .forEach((item) => this.processTouch(item, mousePosition))
  }
  addEdge(edge: Edge) {
    this.edges.push(edge)
  }
  getEdge(id: number) {
    return this.edges.find((edge) => edge.id === id)
  }
  removeEdge(id: number) {
    this.edges = this.edges.filter((edge) => edge.id !== id)
  }
  getEdges() {
    return this.edges
  }
  private getNumberOfChildren(edge: Edge): number {
    if (this.options.hasMouseControl && this.mousePosition) {
      const distanceToMouse = edge.line.distanceTo(this.mousePosition)
      return Math.ceil(
        (this.options.maxFanout - this.options.minFanout) *
          Math.max(0, 1 - distanceToMouse / this.options.mouseEffectRange) +
          this.options.minFanout
      )
    } else {
      return Math.ceil(
        Math.random() * Math.abs(this.options.maxFanout - this.options.minFanout) +
          this.options.minFanout
      )
    }
  }
  grow(): boolean {
    this.edges
      .filter((edge) => !edge.done)
      .filter((edge) =>
        // process mouse induced growth
        this.mousePosition
          ? edge.line.distanceTo(this.mousePosition) < this.options.mouseEffectRange ||
            edge.generation < 5
          : !this.options.hasMouseControl
      )
      .forEach((edge) => {
        edge.completion += this.options.speed

        if (edge.completion > 1) {
          edge.completion = 1
          edge.done = true

          // generate two vectors from the end of the edge going in a direction a random value 10 to 20 degrees away from the edge.
          const childrenCount = this.getNumberOfChildren(edge)
          let startAngle
          let angleStep
          if (childrenCount == 1) {
            startAngle = 0
            angleStep = 0
          } else {
            const widthAngle = this.getRandomAngle()
            startAngle = -widthAngle / 2
            angleStep = widthAngle / (childrenCount - 1)
          }

          for (let i = 0; i < childrenCount; i++) {
            const line1: Line = edge.line
              .continueFrom(startAngle + angleStep * i)
              .toLength(this.options.edgeLength * (Math.random() + 0.5))

            if (
              !this.options.gravity ||
              Math.abs(line1.angleBetween(new Line({ x: 0, y: 0 }, { x: 0, y: -1 }))) < Math.PI / 2
            ) {
              this.addEdge({
                id: this.edges.length,
                line: line1,
                completion: 0,
                done: false,
                svgLine: null,
                generation: edge.generation + 1
              })
            }
          }
        }
      })

    return true
  }

  private getRandomAngle() {
    return (
      ((Math.random() * (this.options.maxAngle - this.options.minAngle) + this.options.minAngle) *
        Math.PI) /
      180
    )
  }

  createBud(edge: Edge, touchpoint: Point) {
    // creates a bud on the edge at the touchpoint. A bud is a new edge that grows from the touchpoint
    const line1: Line = edge.line.continueFromPoint(touchpoint, this.options.maxAngle)
    this.addEdge({
      id: this.edges.length,
      line: line1,
      completion: 0,
      done: false,
      svgLine: null,
      generation: edge.generation + 1
    })
  }

  isDone() {
    if (this.edges.length > this.options.maxEdgeCount) {
      return true
    }
    return this.edges.filter((edge) => !edge.done).length == 0
  }

  isEdgeTouchingAnyOtherEdge(line: Line, exceptId: number): boolean {
    return this.edges.some((edge) => {
      return edge.line.isTouching(line) && edge.id != exceptId
    })
  }

  // Draws the edges which are not completed yet
  drawEdges() {
    this.edges
      .filter((edge) => !edge.done)
      .forEach((edge) => {
        // If the line is not yet drawn, draw it but only part of it, depending on the value of completion
        if (edge.svgLine === null) {
          const strokeAttr = { color: '#000', width: 10 / edge.generation }
          edge.svgLine = this.draw
            .line(
              edge.line.startPoint.x,
              edge.line.startPoint.y,
              edge.line.startPoint.x,
              edge.line.startPoint.y
            )
            .stroke(strokeAttr)
        } else {
          // If the line is already drawn, update it. Take the completion value into account to draw only part of it
          const scaledLine = edge.line.scale(edge.completion)
          if (!this.options.allowOverlap && this.isEdgeTouchingAnyOtherEdge(scaledLine, edge.id)) {
            edge.done = true
          } else {
            edge.svgLine.plot(
              scaledLine.startPoint.x,
              scaledLine.startPoint.y,
              scaledLine.endPoint.x,
              scaledLine.endPoint.y
            )
          }
        }
      })
  }

  animate(position: number) {
    this.edges.forEach((edge) => {
      if (edge.svgLine) {
        edge.svgLine.dmove(Math.sin(position) * 10, 0)
      }
    })
  }
}
